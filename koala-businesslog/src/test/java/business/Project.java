package business;

/**
 * User: zjzhai
 * Date: 12/3/13
 * Time: 10:07 AM
 */
public class Project {
    private String name;

    public Project(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
