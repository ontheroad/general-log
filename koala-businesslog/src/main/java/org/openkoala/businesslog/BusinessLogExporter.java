package org.openkoala.businesslog;

/**
 * User: zjzhai
 * Date: 12/1/13
 * Time: 9:36 PM
 */
public interface BusinessLogExporter {

    BusinessLogExporter export(String log);

}
