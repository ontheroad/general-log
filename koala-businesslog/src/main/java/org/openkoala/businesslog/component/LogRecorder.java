package org.openkoala.businesslog.component;

/**
 * 
 * @author xmfang
 *
 */
public interface LogRecorder {

	void recordLog(String log);
	
}
